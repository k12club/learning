import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email = new FormControl('k12club@hotmail.com', [
    Validators.email,
    Validators.required,
  ])
  password = new FormControl('Pass@1234', [
    Validators.required
  ])
  loginForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private _authService: AuthService
  ) {
    this.loginForm = fb.group({
      email: this.email,
      password: this.password,
    })
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (!this.loginForm.valid) {
      console.log('invalid');
    } else {
      this._authService.login(this.loginForm.value)
      console.log(this.loginForm);
    }

  }
}
