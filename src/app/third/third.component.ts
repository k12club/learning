import { Component, OnInit } from '@angular/core';
import { ThirdService } from '../services/third.service';

@Component({
  selector: 'app-third',
  templateUrl: './third.component.html',
  styleUrls: ['./third.component.css']
})
export class ThirdComponent implements OnInit {
  todos: any[] = []
  constructor(private _thirdService: ThirdService) { }

  ngOnInit(): void {
    this.getAll();
  }
  getAll() {
    this._thirdService.getAll()
      .subscribe(res => {
        this.todos = res
        console.log(res);
      });
  }
}
