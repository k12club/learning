import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FirstComponent } from './first/first.component';
import { LoginComponent } from './login/login.component';
import { SecondComponent } from './second/second.component';
import { AuthGuard } from './services/auth.guard';
import { ThirdComponent } from './third/third.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'first' },
  {
    path: 'first', component: FirstComponent,
    canActivate: [AuthGuard]
  },
  { path: 'second', component: SecondComponent },
  { path: 'third', component: ThirdComponent },
  { path: 'login', component: LoginComponent },
  { path: "**", pathMatch: 'full', redirectTo: 'third' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
