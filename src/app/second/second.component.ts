import { Component, OnInit } from '@angular/core';
import { First } from '../models/first.model';
import { FirstService } from '../services/first.service';


@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {

  todos: First[] = []
  olds: First[] = []
  textSearch: string = ""
  constructor(private _firstService: FirstService) { }

  ngOnInit(): void {
    this.getAll()
  }
  getAll() {
    this._firstService.getAll()
      .subscribe(res => {
        console.log(res);

        this.olds = res
        this.todos = res
      })
  }

  search() {
    this.todos = this.olds.filter(res => {
      return res.title?.includes(this.textSearch)
    })
    // console.log(this.todos);

  }
}
