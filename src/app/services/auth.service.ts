import { Injectable } from '@angular/core';
import { Login, LoginService } from './login.service';
import { JwtHelperService } from '@auth0/angular-jwt'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  currentUser: User = new User()
  isLogedin: boolean = false
  constructor(
    private _loginService: LoginService,
    private _jwtHelper: JwtHelperService,
    private router: Router
  ) {
    const token = localStorage.getItem('token')
    if (token) {
      const decodeUser = this.decodeToken(token)
      this.setCurrentUser(decodeUser)
    }
  }

  login(body: Login) {
    this._loginService.login(body)
      .subscribe((result: any) => {
        localStorage.setItem('token', result.token)
        const decodeUser = this.decodeToken(result.token)
        this.setCurrentUser(decodeUser)
        this.router.navigate(['/'])
      })
  }
  decodeToken(token: string) {
    return this._jwtHelper.decodeToken(token)
  }

  setCurrentUser(decodeUser: User) {
    this.currentUser = decodeUser
    this.isLogedin = true
  }

}
export class User {
  email: string
  name: string
  _id: string
}