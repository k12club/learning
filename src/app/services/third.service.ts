import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ThirdService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<any[]>('http://127.0.0.1:3000/palm')
  }
}
