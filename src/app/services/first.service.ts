import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { First } from '../models/first.model';

@Injectable({
  providedIn: 'root'
})
export class FirstService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<First[]>('https://jsonplaceholder.typicode.com/todos')
  }

  getById(id: number) {
    return this.http.get<First>('https://jsonplaceholder.typicode.com/todos/' + id)
  }


  postTodo(body: First) {
    return this.http.post<First>('https://jsonplaceholder.typicode.com/posts', body)
  }



}
