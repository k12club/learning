import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http: HttpClient
  ) { }

  login(body: Login) {
    return this.http.post('http://127.0.0.1:3000/users/login', body)
  }
}

export interface Login {
  email: string
  password: string
}