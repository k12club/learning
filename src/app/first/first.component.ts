import { Component, OnInit } from '@angular/core';
import { First } from '../models/first.model';
import { FirstService } from '../services/first.service';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {
  input1: number = 0
  input2: number = 0
  output: any
  todos: First[] = []
  todo: First = {}
  body: First = {}

  id: any = 1

  constructor(private _firstService: FirstService) {
    // alert('cont')

  }

  ngOnInit(): void {
    this.getTodo()
    this.getAll()
    // alert('hello angular')
  }

  getTodo() {
    this._firstService.getById(this.id)
      .subscribe(res => {
        this.todo = res
      })
  }

  postTodo() {
    this._firstService.postTodo(this.body)
      .subscribe(res => {
        this.todos = this.todos.concat(res)
      })
  }

  searchTodoById(){
    
  }
  
  getAll() {
    this._firstService.getAll()
      .subscribe(res => {
        this.todos = res
      })
  }
  add() {
    this.input1++
  }

  remove() {
    this.input1--
  }

  onSubmit(input1: number, input2: number) {
    this.output = Number(input1) + Number(input2) == 2 ?
      "A" : Number(input1) + Number(input2)
  }
}
